let noble = require('noble');
let mysql = require('mysql');
let config = require('./libs/config');
let address = require('address');
let fetch = require('node-fetch');

let connection = mysql.createConnection(config.get('mysql'));

let RSSI_THRESHOLD = config.get('RSSI_THRESHOLD');
let EXIT_GRACE_PERIOD = config.get('EXIT_GRACE_PERIOD'); // milliseconds
let CHECK_PERIOD = config.get('CHECK_PERIOD');
let COUNT_ERROR = config.get('COUNT_ERROR');
let SEND_PERIOD = config.get('SEND_PERIOD');
let CHECK_UPDATE_PERIOD = config.get('CHECK_UPDATE_PERIOD');
let MAC = "";

address.mac('eth0', function (err, addr) {
    MAC = addr;
});

console.log(MAC);
toCollector();

let inRange = [];

function getFormattedDate(date) {
    let str = date.getFullYear()
        + "-" + addZero(2, (date.getMonth() + 1))
        + "-" + addZero(2, date.getDate()
            + " " + date.getHours()
            + ":" + date.getMinutes()
            + ":" + date.getSeconds()
            + "." + date.getMilliseconds()
        );
    return str;
}

function addZero(digits_length, source) {
    "use strict"
    let text = source + '';
    while (text.length < digits_length)
        text = '0' + text;
    return text;
}

function toCollector() {
    let sql = "SELECT * FROM rounds WHERE readed = 0 LIMIT 50";
    connection.query(sql, function (err, results, fields) {

        let data = [];
        let forUpdates = [];
        if (results.length < 1)
        {
            return;
        }

        for (let i in results) {
            let startStr = getFormattedDate(results[i].start);
            let endStr = getFormattedDate(results[i].end);
            data.push([
                results[i].mac,
                startStr,
                endStr,
                results[i].rssi,
            ]);
            forUpdates.push(results[i].id);
            // results[i].readed = 1;
        }

        let forSend = {
            mac: MAC,
            data: data
        };
        // let formatData = new FormData();
        let url = config.get('remote');


        fetch(url, {
            method: "POST",
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(forSend)
        })
            .then(function (res) {
                console.log('response status: ', res.status);
                if(res.status == 200)
                {
                    markAsRead(forUpdates);
                }
                else{
                    // console.log('forUpdates: ', forUpdates)
                }
                return res.text();
            })
            .then(function (data) {
                console.log(data)
            })

    })
}

function markAsRead(dataset){
    let sql = "UPDATE rounds SET readed = 1 WHERE id in (?)";
    connection.beginTransaction(function (err) {
        if (err) {
            throw err;
        }
        connection.query(sql, [dataset], function (error, results, fields) {
            if (error) {
                return connection.rollback(function () {
                    console.log('ЧТо-то пошло не так');
                    console.log('err: ', error);
                    // response.sendStatus(400)
                });
            }
            connection.commit(function (err) {
                if (err) {
                    return connection.rollback(function () {
                        // throw err;
                        // response.sendStatus(400)
                    });
                }
                // response.sendStatus(200);
                console.log('success!');
            });
        })
    })
}

function toBase(peripheral) {

    if (peripheral.lastSeen < ( peripheral.startSeen + 10000)) {
        console.log(peripheral.lastSeen - peripheral.startSeen);
        return
    }

    let post = {
        mac: peripheral.peripheral.address,
        start: new Date(peripheral.startSeen),
        end: new Date(peripheral.lastSeen),
        rssi: peripheral.peripheral.rssi
    };

    let query = connection.query('INSERT INTO rounds SET ?', post, function (error, results, fields) {

        if (error) throw error;
    });


}

let update = (response) => {
    let exe = "cd /home/pi/scanner && git stash " +
        "&& git reset --hard origin/master " +
        "&& git pull --force " +
        "&& npm i " +
        "&& pm2 restart all";
    let child = shell.exec(exe, {silent: true, async: true});
    response.sendStatus(200);
    child.stdout.on('data', function (data) {
        // console.log('data: ', );

        sendData(data, 'update');

    });
}

noble.on('discover', function (peripheral) {

    if (peripheral.rssi < RSSI_THRESHOLD) {
        // ignore
        return;
    }
    // console.log(JSON.stringify(peripheral.advertisement.serviceData));
    let id = peripheral.id;
    let entered = !inRange[id];

    if (entered) {
        inRange[id] = {
            peripheral: peripheral
        };

        console.log('[' + new Date() + '] [' + peripheral.address + '] entered (RSSI ' + peripheral.rssi + ') ');
        inRange[id].startSeen = Date.now();
    }

    inRange[id].lastSeen = Date.now();
    inRange[id].count = 0;
});
noble.on('stateChange', function (state) {
    if (state === 'poweredOn') {
        noble.startScanning([], true);
    } else {
        noble.stopScanning();
    }
});

setInterval(toCollector, SEND_PERIOD);
// setInterval(update, CHECK_UPDATE_PERIOD);
setInterval(function () {
    for (let id in inRange) {
        // console.log((inRange[id].lastSeen - inRange[id].startSeen)/1000);
        // console.log(inRange[id]);
        //Если последний раз мы его видели меньше чем сейчас
        // за вычетом периода EXIT_GRACE_PERIOD (то есть более чем две секунды назад по дефолту)
        if (inRange[id].lastSeen < (Date.now() - EXIT_GRACE_PERIOD)) {
            inRange[id].count++;
            // console.log(inRange[id].count);
            if (inRange[id].count > COUNT_ERROR) {
                let peripheral = inRange[id].peripheral;

                console.log('[' + new Date() + '] [' + peripheral.address + '] exited (RSSI ' + peripheral.rssi + ') '
                    + 'Start seen: ' + new Date(inRange[id].startSeen)
                    + ' last seen: ' + (Date.now() - inRange[id].lastSeen) / 1000 + ' секунд');
                toBase(inRange[id]);
                delete inRange[id];
            }

        }
        else {
            // let peripheral = inRange[id].peripheral;
            // console.log('[' + Date.now() + ']"'  + '"[' + peripheral.address + '] not exited (RSSI ' + peripheral.rssi + ') ' + new Date());
        }
    }
// }, EXIT_GRACE_PERIOD / 4);
}, CHECK_PERIOD);
